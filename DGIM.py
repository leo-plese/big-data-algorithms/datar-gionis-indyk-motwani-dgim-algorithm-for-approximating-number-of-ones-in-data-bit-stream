from sys import stdin

def get_new_dict(size_to_blocks_dict, block_size):
    if len(size_to_blocks_dict[block_size]) == 3:
        cur_blocks = size_to_blocks_dict[block_size]
        new_block = cur_blocks[1]
        cur_blocks = [cur_blocks[-1]]
        size_to_blocks_dict[block_size] = cur_blocks

        new_blocks = size_to_blocks_dict.get(block_size*2, [])
        new_blocks.append(new_block)
        size_to_blocks_dict[block_size*2] = new_blocks

        get_new_dict(size_to_blocks_dict, block_size*2)

    return size_to_blocks_dict

def get_cands(size_to_blocks_dict, cur_size, t_lim):
    cands = []
    max_size = max(size_to_blocks_dict.keys())

    while cur_size <= max_size:

        cur_elems = size_to_blocks_dict[cur_size][::-1]
        for i in range(len(cur_elems)):
            cur_elem = cur_elems[i]

            if cur_elem > t_lim:
                cands.append(cur_size)
            else:
                return cands

        cur_size *= 2

    return cands


def get_number_ones(size_to_blocks_dict, t, k):
    t_lim = t - 1 - k

    if len(size_to_blocks_dict.keys()) > 0:
        cur_size = min(size_to_blocks_dict.keys())

        cands = get_cands(size_to_blocks_dict, cur_size, t_lim)

        if len(cands) > 0:
            ones_count = sum(cands[:-1]) + cands[-1]//2
        else:
            ones_count = 0

        return ones_count
    else:
        return 0



if __name__=="__main__":
    lines = stdin.readlines()

    N = int(lines[0].strip())

    lines = lines[1:]

    size_to_blocks_dict = {}

    t = 0
    for line in lines:
        line = line.strip()
        if line.startswith("q"):
            k = int((line.split()[1]).strip())

            print(get_number_ones(size_to_blocks_dict, t, k))


        else:
            for c in line:
                if len(size_to_blocks_dict.keys()) > 0:
                    cur_size = max(size_to_blocks_dict.keys())
                    while cur_size > 0:
                        cur_block_elems = size_to_blocks_dict[cur_size]

                        new_cur_block_elems = []

                        for cur_elem_ind in range(len(cur_block_elems)):
                            cur_elem = cur_block_elems[cur_elem_ind]
                            if cur_elem > (t-N):
                                new_cur_block_elems.append(cur_elem)

                        size_to_blocks_dict[cur_size] = new_cur_block_elems

                        cur_size //= 2

                if c == "1":
                    new_block_one = t  # t_end
                    one_blocks = size_to_blocks_dict.get(1, [])
                    one_blocks.append(new_block_one)
                    size_to_blocks_dict[1] = one_blocks

                    size_to_blocks_dict = get_new_dict(size_to_blocks_dict, 1)

                t += 1
