# Datar-Gionis-Indyk-Motwani DGIM Algorithm for Approximating Number of Ones in Data Bit Stream

Implemented in Python.

My lab assignment in Analysis of Massive Datasets, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: 2021